<?php
define('URL', 'http://api.new-project.com');
define('LOG_FILE', 'proxy_error.log');


//declare getallheaders for nginx compatibility
if (!function_exists('getallheaders')) {
    function getallheaders()
    {
        $headers = [];
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}


function logger($msg, $context = array())
{
    $data = '[' . date('Y-m-d H:i:s') . '][' . $_SERVER['REMOTE_ADDR'] . '] ';
    $replace = array();
    foreach ($context as $key => $val) {
        if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
            $replace['{' . $key . '}'] = $val;
        }
    }

    $data .= strtr($msg, $replace) . "\n";

    file_put_contents(LOG_FILE, $data, FILE_APPEND);
}

function proxy()
{
    $method = $_SERVER['REQUEST_METHOD'];
    $url = URL . $_SERVER['REQUEST_URI'];
    //получаем заголовки от моб клиента, браузер и прочих
    $headers = getallheaders();
    $headers_str = [];

    unset($headers['Host']);
    foreach ($headers as $key => $value) {
        $headers_str[] = $key . ":" . $value;
    }

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_URL, $url);

    //тут только основные методы
    if (in_array($method, ['PUT', 'PATCH', 'POST', 'DELETE'])) {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        $request_params = ($_POST);
        if (empty($request_params)) {
            $php_input = file_get_contents('php://input');
            $request_params = empty($php_input) ? null : $php_input;
        }

        logger("{context} : {$_SERVER['REQUEST_URI']}", ['context' => 'URL']);
        logger("{context} : " . print_r($request_params, 1), ['context' => 'REQUEST']);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $request_params);
    }

    if (!empty($_COOKIE)) {
        $cookie = array();
        foreach ($_COOKIE as $key => $value) {
            $cookie[] = $key . '=' . $value;
        }
        $cookie = implode('; ', $cookie);
        curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    }

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers_str);
    $response = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);

    logger("{context} : $response", ['context' => 'RESPONSE']);

    if ($response === false) {
        $errno = curl_errno($ch);
        $error = curl_error($ch);
        print_r("Site temporary unavailable, returned error $errno: $error\n");
        die;
    }

    header('Content-Type: ' . $info['content_type']);
    http_response_code($info['http_code']);
    echo $response;
}

proxy();